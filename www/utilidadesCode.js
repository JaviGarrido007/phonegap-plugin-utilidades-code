var exec = require('cordova/exec');
module.exports = {

    salirAppIos: function(askUser)
    {
        if (!askUser) 
        {
            askUser = false;
        }

        exec(
            function callback(data) 
            {
                console.log("Retorno correcto");
            },
            function errorHandler(err)
            {
                console.log("Error");
            },
            'UtilidadesCode',
            'salirAppIos', 
            [askUser]
        );
    },
    obtenerIdCode: function(success, fail)
    {
        cordova.exec(success, fail, 'UtilidadesCode', 'getCodeId', []);
    }

};