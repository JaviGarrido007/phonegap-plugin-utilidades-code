#import "UtilidadesCode.h"
#import <Cordova/CDV.h>
#import "UICKeyChainStore.h"

@implementation UtilidadesCode

- (void)salirAppIos:(CDVInvokedUrlCommand*)command
{
    
    CDVPluginResult* pluginResult = nil;
    
    NSString* echo = [command.arguments objectAtIndex:0];
    BOOL popUpAlert = [echo boolValue];
    bool closeApp = false;
    
    if (popUpAlert)
    {
        UIAlertView *confirmationPopUp = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                                    message:@"Do you really want to close the application?"
                                                                   delegate:self
                                                          cancelButtonTitle:@"No"
                                                          otherButtonTitles:@"Yes", nil];
        [confirmationPopUp show];
    } 
    else
    {
        closeApp = YES;
    }
    
    if (closeApp)
    {
       [self exitApplication];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex)
    {
        case 0: //"No" pressed
        break;
        case 1: //"Yes" pressed
        [self exitApplication];
        break;
    }
}

- (void) exitApplication
{
    exit(0);
}

///////////////////////////////////////////////////////////


-(void)getCodeId:(CDVInvokedUrlCommand*)command
{
    [self.commandDelegate runInBackground:^{
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *uuidUserDefaults = [defaults objectForKey:@"uuid"];
        
        NSString *uuid = [UICKeyChainStore stringForKey:@"uuid"];

        if ( uuid && !uuidUserDefaults) {
            [defaults setObject:uuid forKey:@"uuid"];
            [defaults synchronize];
            
        }  else if ( !uuid && !uuidUserDefaults ) {
            NSString *uuidString = [[NSUUID UUID] UUIDString];
            
            [UICKeyChainStore setString:uuidString forKey:@"uuid"];
            
            [defaults setObject:uuidString forKey:@"uuid"];
            [defaults synchronize];
            
            uuid = [UICKeyChainStore stringForKey:@"uuid"];
            
        } else if ( ![uuid isEqualToString:uuidUserDefaults] ) {
            [UICKeyChainStore setString:uuidUserDefaults forKey:@"uuid"];
            uuid = [UICKeyChainStore stringForKey:@"uuid"];
        }

        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:uuid];
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }];
}

@end