//
//  CDVUniqueDeviceID.h
//
//
//
    
#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

@interface CDVUniqueDeviceID : CDVPlugin

- (void)get_code:(CDVInvokedUrlCommand*)command;

@end