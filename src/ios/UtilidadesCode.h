/********* Echo.h Cordova Plugin Header *******/
#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

@interface

UtilidadesCode : CDVPlugin

- (void)salirAppIos:(CDVInvokedUrlCommand*)command;

- (void)getCodeId:(CDVInvokedUrlCommand*)command;

@end
