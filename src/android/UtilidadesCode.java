package com.codedonostia.plugins;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.provider.Settings.Secure;
// import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.lang.reflect.Method;

public class UtilidadesCode extends CordovaPlugin
{
    public static final String TAG = "UtilidadesCode";
    public CallbackContext callbackContext;
    public static final int REQUEST_READ_PHONE_STATE = 0;
    protected final static String permission = Manifest.permission.READ_PHONE_STATE;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException 
    {
        this.callbackContext = callbackContext;

        try 
        {
            if(action.equals("getCodeId"))
            {
                if(this.hasPermission(permission))
                {
                    getCodeId();
                }
                else
                {
                    this.requestPermission(this, REQUEST_READ_PHONE_STATE, permission);
                }
            }
            else
            {
                this.callbackContext.error("Accion No Valida");
                return false;
            }
        }
        catch(Exception e ) 
        {
            this.callbackContext.error("Excepcion: ".concat(e.getMessage()));
            return false;
        }

        return true;

    }

    ////////////////////////////////////

    protected void getCodeId()
    {
        try 
        {
            Context context = cordova.getActivity().getApplicationContext();
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            String deviceUniqueIdentifier;
            
            // String simID = tm.getSimSerialNumber();

            if (tm.getDeviceId() != null)
            {
                deviceUniqueIdentifier = tm.getDeviceId();
            }
            else if (tm.getImei() != null)
            {
                deviceUniqueIdentifier = tm.getImei();
            }
            else
            {
                deviceUniqueIdentifier = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
            }

            deviceUniqueIdentifier = "CdOd-" + deviceUniqueIdentifier;
            this.callbackContext.success(deviceUniqueIdentifier);
        }
        catch(Exception e )
        {
            this.callbackContext.error("Exception occurred: ".concat(e.getMessage()));
        }
    }

    ////////////////////////////////////////////

    private boolean hasPermission(String permission) throws Exception
    {
        boolean hasPermission = true;
        Method method = null;

        try
        {
            method = cordova.getClass().getMethod("hasPermission", permission.getClass());
            Boolean bool = (Boolean) method.invoke(cordova, permission);
            hasPermission = bool.booleanValue();
        }
        catch (NoSuchMethodException e)
        {
            Log.w(TAG, "Cordova v" + CordovaWebView.CORDOVA_VERSION + " does not support API 23 runtime permissions so defaulting to GRANTED for " + permission);
        }

        return hasPermission;
    }

    private void requestPermission(CordovaPlugin plugin, int requestCode, String permission) throws Exception
    {
        try 
        {
            java.lang.reflect.Method method = cordova.getClass().getMethod("requestPermission", org.apache.cordova.CordovaPlugin.class ,int.class, java.lang.String.class);
            method.invoke(cordova, plugin, requestCode, permission);
        }
        catch (NoSuchMethodException e)
        {
            throw new Exception("requestPermission() method not found in CordovaInterface implementation of Cordova v" + CordovaWebView.CORDOVA_VERSION);
        }
    }

    @Override
    public void onRequestPermissionResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_PHONE_STATE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    getCodeId();
                }
                break;

            default:
                break;
        }
    }
}